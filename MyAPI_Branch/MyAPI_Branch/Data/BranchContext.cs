﻿using Microsoft.EntityFrameworkCore;

namespace MyAPI_Branch.Data
{
    public class BranchContext : DbContext
    {
        public BranchContext(DbContextOptions<BranchContext> option) : base(option)
        { }

        public DbSet<Branch>? Branchs { get; set; }
    }
}
